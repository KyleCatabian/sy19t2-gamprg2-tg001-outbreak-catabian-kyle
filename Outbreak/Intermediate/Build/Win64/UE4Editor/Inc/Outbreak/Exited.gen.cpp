// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Outbreak/Exited.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeExited() {}
// Cross Module References
	OUTBREAK_API UFunction* Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Outbreak();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter_NoRegister();
	OUTBREAK_API UClass* Z_Construct_UClass_UExited_NoRegister();
	OUTBREAK_API UClass* Z_Construct_UClass_UExited();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics
	{
		struct _Script_Outbreak_eventExited_Parms
		{
			ACharacter* HitCharacter;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HitCharacter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics::NewProp_HitCharacter = { UE4CodeGen_Private::EPropertyClass::Object, "HitCharacter", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(_Script_Outbreak_eventExited_Parms, HitCharacter), Z_Construct_UClass_ACharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics::NewProp_HitCharacter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Exited.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Outbreak, "Exited__DelegateSignature", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00130000, sizeof(_Script_Outbreak_eventExited_Parms), Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UExited::StaticRegisterNativesUExited()
	{
	}
	UClass* Z_Construct_UClass_UExited_NoRegister()
	{
		return UExited::StaticClass();
	}
	struct Z_Construct_UClass_UExited_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnExited_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnExited;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UExited_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_Outbreak,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExited_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Exited.h" },
		{ "ModuleRelativePath", "Exited.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExited_Statics::NewProp_OnExited_MetaData[] = {
		{ "Category", "Test" },
		{ "ModuleRelativePath", "Exited.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UExited_Statics::NewProp_OnExited = { UE4CodeGen_Private::EPropertyClass::MulticastDelegate, "OnExited", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000010080000, 1, nullptr, STRUCT_OFFSET(UExited, OnExited), Z_Construct_UDelegateFunction_Outbreak_Exited__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UExited_Statics::NewProp_OnExited_MetaData, ARRAY_COUNT(Z_Construct_UClass_UExited_Statics::NewProp_OnExited_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UExited_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UExited_Statics::NewProp_OnExited,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UExited_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UExited>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UExited_Statics::ClassParams = {
		&UExited::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_UExited_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UExited_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UExited_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UExited_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UExited()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UExited_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UExited, 1334604760);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UExited(Z_Construct_UClass_UExited, &UExited::StaticClass, TEXT("/Script/Outbreak"), TEXT("UExited"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UExited);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
