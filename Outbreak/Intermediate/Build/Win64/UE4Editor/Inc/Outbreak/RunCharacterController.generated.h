// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OUTBREAK_RunCharacterController_generated_h
#error "RunCharacterController.generated.h already included, missing '#pragma once' in RunCharacterController.h"
#endif
#define OUTBREAK_RunCharacterController_generated_h

#define Outbreak_Source_Outbreak_RunCharacterController_h_15_RPC_WRAPPERS
#define Outbreak_Source_Outbreak_RunCharacterController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Outbreak_Source_Outbreak_RunCharacterController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Outbreak"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define Outbreak_Source_Outbreak_RunCharacterController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Outbreak"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define Outbreak_Source_Outbreak_RunCharacterController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunCharacterController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunCharacterController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public:


#define Outbreak_Source_Outbreak_RunCharacterController_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunCharacterController)


#define Outbreak_Source_Outbreak_RunCharacterController_h_15_PRIVATE_PROPERTY_OFFSET
#define Outbreak_Source_Outbreak_RunCharacterController_h_12_PROLOG
#define Outbreak_Source_Outbreak_RunCharacterController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Outbreak_Source_Outbreak_RunCharacterController_h_15_PRIVATE_PROPERTY_OFFSET \
	Outbreak_Source_Outbreak_RunCharacterController_h_15_RPC_WRAPPERS \
	Outbreak_Source_Outbreak_RunCharacterController_h_15_INCLASS \
	Outbreak_Source_Outbreak_RunCharacterController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Outbreak_Source_Outbreak_RunCharacterController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Outbreak_Source_Outbreak_RunCharacterController_h_15_PRIVATE_PROPERTY_OFFSET \
	Outbreak_Source_Outbreak_RunCharacterController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Outbreak_Source_Outbreak_RunCharacterController_h_15_INCLASS_NO_PURE_DECLS \
	Outbreak_Source_Outbreak_RunCharacterController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Outbreak_Source_Outbreak_RunCharacterController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
