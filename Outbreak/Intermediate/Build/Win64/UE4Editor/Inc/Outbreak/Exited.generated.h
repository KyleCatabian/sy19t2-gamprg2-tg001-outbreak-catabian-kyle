// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ACharacter;
#ifdef OUTBREAK_Exited_generated_h
#error "Exited.generated.h already included, missing '#pragma once' in Exited.h"
#endif
#define OUTBREAK_Exited_generated_h

#define Outbreak_Source_Outbreak_Exited_h_13_DELEGATE \
struct _Script_Outbreak_eventExited_Parms \
{ \
	ACharacter* HitCharacter; \
}; \
static inline void FExited_DelegateWrapper(const FMulticastScriptDelegate& Exited, ACharacter* HitCharacter) \
{ \
	_Script_Outbreak_eventExited_Parms Parms; \
	Parms.HitCharacter=HitCharacter; \
	Exited.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Outbreak_Source_Outbreak_Exited_h_18_RPC_WRAPPERS
#define Outbreak_Source_Outbreak_Exited_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Outbreak_Source_Outbreak_Exited_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUExited(); \
	friend struct Z_Construct_UClass_UExited_Statics; \
public: \
	DECLARE_CLASS(UExited, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Outbreak"), NO_API) \
	DECLARE_SERIALIZER(UExited)


#define Outbreak_Source_Outbreak_Exited_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUExited(); \
	friend struct Z_Construct_UClass_UExited_Statics; \
public: \
	DECLARE_CLASS(UExited, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Outbreak"), NO_API) \
	DECLARE_SERIALIZER(UExited)


#define Outbreak_Source_Outbreak_Exited_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UExited(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UExited) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExited); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExited); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExited(UExited&&); \
	NO_API UExited(const UExited&); \
public:


#define Outbreak_Source_Outbreak_Exited_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UExited(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExited(UExited&&); \
	NO_API UExited(const UExited&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExited); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExited); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UExited)


#define Outbreak_Source_Outbreak_Exited_h_18_PRIVATE_PROPERTY_OFFSET
#define Outbreak_Source_Outbreak_Exited_h_15_PROLOG
#define Outbreak_Source_Outbreak_Exited_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Outbreak_Source_Outbreak_Exited_h_18_PRIVATE_PROPERTY_OFFSET \
	Outbreak_Source_Outbreak_Exited_h_18_RPC_WRAPPERS \
	Outbreak_Source_Outbreak_Exited_h_18_INCLASS \
	Outbreak_Source_Outbreak_Exited_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Outbreak_Source_Outbreak_Exited_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Outbreak_Source_Outbreak_Exited_h_18_PRIVATE_PROPERTY_OFFSET \
	Outbreak_Source_Outbreak_Exited_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Outbreak_Source_Outbreak_Exited_h_18_INCLASS_NO_PURE_DECLS \
	Outbreak_Source_Outbreak_Exited_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Outbreak_Source_Outbreak_Exited_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
