// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OUTBREAK_RunCharacter_generated_h
#error "RunCharacter.generated.h already included, missing '#pragma once' in RunCharacter.h"
#endif
#define OUTBREAK_RunCharacter_generated_h

#define Outbreak_Source_Outbreak_RunCharacter_h_12_RPC_WRAPPERS
#define Outbreak_Source_Outbreak_RunCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Outbreak_Source_Outbreak_RunCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunCharacter(); \
	friend struct Z_Construct_UClass_ARunCharacter_Statics; \
public: \
	DECLARE_CLASS(ARunCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Outbreak"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacter)


#define Outbreak_Source_Outbreak_RunCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesARunCharacter(); \
	friend struct Z_Construct_UClass_ARunCharacter_Statics; \
public: \
	DECLARE_CLASS(ARunCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Outbreak"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacter)


#define Outbreak_Source_Outbreak_RunCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacter(ARunCharacter&&); \
	NO_API ARunCharacter(const ARunCharacter&); \
public:


#define Outbreak_Source_Outbreak_RunCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacter(ARunCharacter&&); \
	NO_API ARunCharacter(const ARunCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunCharacter)


#define Outbreak_Source_Outbreak_RunCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StaticMesh() { return STRUCT_OFFSET(ARunCharacter, StaticMesh); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(ARunCharacter, Camera); } \
	FORCEINLINE static uint32 __PPO__CameraArm() { return STRUCT_OFFSET(ARunCharacter, CameraArm); }


#define Outbreak_Source_Outbreak_RunCharacter_h_9_PROLOG
#define Outbreak_Source_Outbreak_RunCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Outbreak_Source_Outbreak_RunCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Outbreak_Source_Outbreak_RunCharacter_h_12_RPC_WRAPPERS \
	Outbreak_Source_Outbreak_RunCharacter_h_12_INCLASS \
	Outbreak_Source_Outbreak_RunCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Outbreak_Source_Outbreak_RunCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Outbreak_Source_Outbreak_RunCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Outbreak_Source_Outbreak_RunCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Outbreak_Source_Outbreak_RunCharacter_h_12_INCLASS_NO_PURE_DECLS \
	Outbreak_Source_Outbreak_RunCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Outbreak_Source_Outbreak_RunCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
