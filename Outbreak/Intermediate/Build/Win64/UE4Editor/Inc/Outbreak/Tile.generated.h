// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OUTBREAK_Tile_generated_h
#error "Tile.generated.h already included, missing '#pragma once' in Tile.h"
#endif
#define OUTBREAK_Tile_generated_h

#define Outbreak_Source_Outbreak_Tile_h_12_RPC_WRAPPERS
#define Outbreak_Source_Outbreak_Tile_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Outbreak_Source_Outbreak_Tile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Outbreak"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define Outbreak_Source_Outbreak_Tile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Outbreak"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define Outbreak_Source_Outbreak_Tile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public:


#define Outbreak_Source_Outbreak_Tile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATile)


#define Outbreak_Source_Outbreak_Tile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StaticMesh() { return STRUCT_OFFSET(ATile, StaticMesh); } \
	FORCEINLINE static uint32 __PPO__Floor() { return STRUCT_OFFSET(ATile, Floor); } \
	FORCEINLINE static uint32 __PPO__LeftWall() { return STRUCT_OFFSET(ATile, LeftWall); } \
	FORCEINLINE static uint32 __PPO__RightWall() { return STRUCT_OFFSET(ATile, RightWall); } \
	FORCEINLINE static uint32 __PPO__AttachPoint() { return STRUCT_OFFSET(ATile, AttachPoint); } \
	FORCEINLINE static uint32 __PPO__ExitTrigger() { return STRUCT_OFFSET(ATile, ExitTrigger); }


#define Outbreak_Source_Outbreak_Tile_h_9_PROLOG
#define Outbreak_Source_Outbreak_Tile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Outbreak_Source_Outbreak_Tile_h_12_PRIVATE_PROPERTY_OFFSET \
	Outbreak_Source_Outbreak_Tile_h_12_RPC_WRAPPERS \
	Outbreak_Source_Outbreak_Tile_h_12_INCLASS \
	Outbreak_Source_Outbreak_Tile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Outbreak_Source_Outbreak_Tile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Outbreak_Source_Outbreak_Tile_h_12_PRIVATE_PROPERTY_OFFSET \
	Outbreak_Source_Outbreak_Tile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Outbreak_Source_Outbreak_Tile_h_12_INCLASS_NO_PURE_DECLS \
	Outbreak_Source_Outbreak_Tile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Outbreak_Source_Outbreak_Tile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
