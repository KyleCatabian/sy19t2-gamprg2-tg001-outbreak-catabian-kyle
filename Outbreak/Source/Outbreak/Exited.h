// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Exited.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExited, class ACharacter*, HitCharacter);

UCLASS()
class OUTBREAK_API UExited : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = "Test")
		FExited OnExited;
	
};
