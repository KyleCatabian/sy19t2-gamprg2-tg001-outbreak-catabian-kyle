// Fill out your copyright notice in the Description page of Project Settings.

#include "Tile.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"


// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("Static Mesh");
	SetRootComponent(StaticMesh);

	Floor = CreateDefaultSubobject<UStaticMeshComponent>("Floor Mesh");
	Floor->SetupAttachment(StaticMesh);

	LeftWall = CreateDefaultSubobject<UStaticMeshComponent>("Left Wall Mesh");
	LeftWall->SetupAttachment(StaticMesh);

	RightWall = CreateDefaultSubobject<UStaticMeshComponent>("Right Wall Mesh");
	RightWall->SetupAttachment(StaticMesh);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>("Attach Point");
	AttachPoint->SetupAttachment(StaticMesh);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>("Exit Trigger");
	ExitTrigger->SetupAttachment(StaticMesh);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

