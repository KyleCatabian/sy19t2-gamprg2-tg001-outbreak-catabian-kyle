// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class OUTBREAK_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

private:
	class ARunCharacter* runCharacter;

public:
	ARunCharacterController();

protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

	void MoveForward(float scale);
	void MoveRight(float scale);

public:
	virtual void Tick(float DeltaTime) override;
	
};
