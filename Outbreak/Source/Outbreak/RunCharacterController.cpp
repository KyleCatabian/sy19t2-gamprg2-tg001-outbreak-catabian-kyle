// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacterController.h"
#include "RunCharacter.h"

ARunCharacterController::ARunCharacterController()
{
}

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();
	runCharacter = Cast<ARunCharacter>(GetPawn());

}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveForward", this, &ARunCharacterController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}

void ARunCharacterController::MoveForward(float scale)
{
	const FRotator Rotation = GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

	runCharacter->AddMovementInput(Direction, 1);
}

void ARunCharacterController::MoveRight(float scale)
{
	if (scale != 0.0f)
	{
		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		runCharacter->AddMovementInput(Direction, scale);
	}
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
